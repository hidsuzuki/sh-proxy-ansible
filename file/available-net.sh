#!/bin/bash

/usr/bin/firewall-cmd --set-default-zone public
/usr/bin/firewall-cmd --reload
/usr/bin/systemctl restart squid