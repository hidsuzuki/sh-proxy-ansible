#!/bin/bash

/usr/bin/firewall-cmd --set-default-zone work
/usr/bin/firewall-cmd --reload
/usr/bin/systemctl restart squid